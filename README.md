# InteractiveDictionary

## Description
Project represents small interactive dictionary. User writes word in which is interested and program return the meanings of the the word. In case of little typo error, program asks user if he meant other word. If word is not in database or does not exist, program alerts user about it. Project works in CLI.

## Motivation
Project was created as a hobby project during the course Build 10 Real World Applications within Udemy platform. The course was held in online form. More information about the course: https://www.udemy.com/the-python-mega-course.

## Installation
You need Python 3 to run program.

No other modules are necessary to install.

## Usage
To run the program, use:

``python dictionary.py``

### Examples:
```
Enter word: hockey
Game in which two teams of six players hit a puck with a curved stick, and shoot it in the net, which means that team scores.
A form of hockey played on an ice rink with a puck rather than ball.

Enter word: rainn
Did you mean 'rain'? Enter Y if yes or N if no: Y
Precipitation in the form of liquid water drops with diameters greater than 0.5 millimetres.
To fall from the clouds in drops of water.

Enter word: fdsmfkds
The word doesn't exist. Please check it.
```

## Contributing
Any ideas how to improve project is welcomed. You can create issue here or write message to the email.

## Credits
Created by Tomas Cernak.

Email: cernak.tomi@gmail.com

Released: 4.11.2018

## To do
testing
