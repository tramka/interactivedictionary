import json
from difflib import get_close_matches

database = json.load(open('data.json', 'r'))

def translate(word):
    '''
    Looks for meaning of word in database and returns the result. In case
    of typo error, module difflib is used to check whether user made
    error.

    Parameters
    ----------
    word : <str>
        Provided word by user.

    Returns
    -------
    <str> or <list>
        String is returned in case of wrong input or if there is just only
        one meaning for word, otherwise list of meanings is returned.
    '''

    word = word.lower()
    if word in database:
        return database[word]
    elif word.title() in database:
        return database[word.title()]
    elif word.upper() in database:
        return database[word.upper()]
    else:
        similar_words = get_close_matches(word, database.keys(), cutoff=0.8)
        if len(similar_words) > 0:
            yn = input("Did you mean '{}'? Enter Y if yes or N if no: ".format(similar_words[0]))
            if yn == 'Y':
                return database[similar_words[0]]
            elif yn == 'N':
                return 'The word doesn\'t exist. Please check it.'
            else:
                return 'We did not understand your query.'
        else:
            return 'The word doesn\'t exist. Please check it.'

def main():
    '''
    Orchestrates the whole process. Get word from user and returns the
    meanings of provided word.
    '''

    word = input('Enter word: ')
    output = translate(word)

    if type(output) == list:
        for item in output:
            print(item)
    else:
        print(output)

if __name__ == '__main__':

    main()
